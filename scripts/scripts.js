!function(n){n.module("ngLoad",["ng"]).directive("ngLoad",["$parse",function(n){return{restrict:"A",compile:function(o,t){var e=n(t.ngLoad);return function(n,o){o.on("load",function(o){n.$apply(function(){e(n,{$event:o})})})}}}}])}(angular);

!function(){"use strict";function n(n,e){function i(i){function t(){e.cancel(r),r=e(s,a)}var r,u=i.scope,c=i.debounce,o=angular.element(n),a=c||0===c?c:100,s="function"==typeof i.resizedFn?i.resizedFn:null;t(),o.bind("resize",t),u&&u.$on("$destroy",function(){o.unbind("resize",t)})}function t(n){var e=Object.create(r);return e.buildInstance(n),e}var r={buildInstance:i};return{init:t}}angular.module("resize",[]),angular.module("resize").factory("resize",n),n.$inject=["$window","$timeout"]}();

var showroom = angular.module("Showroom", ["ngAnimate", "ngLoad", "resize", "duScroll"]);

showroom.controller("ShowroomController", function ($http, $filter, $scope, $window, $location, $timeout, $interval, $q, resize) {
	$scope.length = function (obj) {
		return Object.keys(obj).length;
	}

	$scope[$location.path().slice(1)] = true;

	$scope.scene = {
		componentOrder: ['a', 'c', 't', 'b', 'f']
	};

	$scope.styles = {
		color: {
			name: "Fingerprint-Resistant Finish",
			options: {
				"sunset-bronze": {
					name: "Sunset Bronze",
					color: "#cabcad",
					handles: {
						"contemporary-handle": "u"
					}
				},
				"stainless-steel": {
					name: "Stainless Steel",
					color: "#b8b8b8",
					handles: {
						"contemporary-handle": "t",
						"curved-handle": "s"
					}
				},
				"black-stainless": {
					name: "Black Stainless",
					color: "#3a3732",
					handles: {
						"curved-handle": "b"
					}
				}
			}
		},
		handle: {
			name: "Design Details",
			options: {
				"contemporary-handle": {
					name: "Contemporary Handle&nbsp;& Barrel&nbsp;Knobs",
					color: "#797979"
				},
				"curved-handle": {
					name: "Curved Handle &&nbsp;Bladed&nbsp;Knobs",
					color: "#747474"
				}
			}
		}
	};

	$scope.themes = {
		modern: {
			name: "Modern",
			components: {
				c: {
					name: "Cabinet Color",
					mobileName: "Cabinets",
					options: {
						b: {
							name: "Dark Gray",
							color: "#2c2721"
						},
						o: {
							name: "Oak",
							color: "#987f66"
						},
						w: {
							name: "White",
							color: "#dfdad6"
						},
						n: {
							name: "White/Walnut",
							color: "#9b887d"
						}
					},
					mask: "mask.png"
				},
				f: {
					name: "Flooring",
					options: {
						w: {
							name: "Walnut",
							color: "#3f2511"
						},
						o: {
							name: "Oak",
							color: "#845d33"
						},
						s: {
							name: "Slate Tile",
							color: "#847b72"
						},
						p: {
							name: "Porcelain Tile",
							color: "#b2a9a2"
						}
					},
					mask: "mask.png"
				},
				b: {
					name: "Backsplash",
					options: {
						s: {
							name: "Scalloped",
							color: "#d2cbc4"
						},
						h: {
							name: "Herringbone",
							color: "#d9d0c7"
						},
						g: {
							name: "Glass Mosaic",
							color: "#9c97af"
						},
						b: {
							name: "Beveled Subway",
							color: "#d9d2cc"
						}
					},
					mask: "mask.png"
				},
				t: {
					name: "Countertops",
					options: {
						q: {
							name: "Black Quartz",
							color: "#3b3229"
						},
						c: {
							name: "Concrete",
							color: "#bdb4aa"
						},
						b: {
							name: "Butcher Block",
							color: "#bd9b79"
						},
						w: {
							name: "White Quartz",
							color: "#d4cbc3"
						}
					},
					mask: "mask.png"
				},
				a: {
					name: "Appliance Style",
					options: {
						u: {
							name: "Sunset Bronze",
							color: "#cabcad",
							suboptions: {
								u: {
									slug: "contemporary-handle",
									name: "Contemporary Handle&nbsp;& Barrel&nbsp;Knobs",
									mask: "contemporary-handle-mask.png"
								}
							}
						},
						s: {
							name: "Stainless Steel",
							color: "#b8b8b8",
							suboptions: {
								t: {
									slug: "contemporary-handle",
									name: "Contemporary Handle&nbsp;& Barrel&nbsp;Knobs",
									mask: "contemporary-handle-mask.png"
								},
								s: {
									slug: "curved-handle",
									name: "Curved Handle &&nbsp;Bladed&nbsp;Knobs",
									mask: "curved-handle-mask.png"
								}
							}
						},
						b: {
							name: "Black Stainless",
							color: "#3a3732",
							suboptions: {
								b: {
									slug: "curved-handle",
									name: "Curved Handle &&nbsp;Bladed&nbsp;Knobs",
									mask: "curved-handle-mask.png"
								}
							}
						}
					},
					suboptions: true
				}
			},
			preparedScenes: [
				{
					c: "w",
					f: "s",
					b: "s",
					t: "q",
					// a: "u"
				},
				{
					c: "b",
					f: "w",
					b: "g",
					t: "w",
					// a: "u"
				},
				{
					c: "n",
					f: "p",
					b: "h",
					t: "w",
					// a: "u"
				}
			],
			maxWidth: 4025,
			maxHeight: 3333
		},
		traditional: {
			name: "Traditional",
			components: {
				f: {
					name: "Flooring",
					options: {
						w: {
							name: "Dark Wood",
							color: "#4a382c"
						},
						l: {
							name: "Light Wood",
							color: "#a0896e"
						},
						d: {
							name: "Driftwood",
							color: "#bfb5ab"
						},
						s: {
							name: "Stone",
							color: "#857d78"
						}
					},
					mask: "mask.png"
				},
				t: {
					name: "Countertops",
					options: {
						b: {
							name: "Black",
							color: "#030303"
						},
						c: {
							name: "Concrete",
							color: "#b0afab"
						},
						d: {
							name: "Dark Gray",
							color: "#171719"
						},
						w: {
							name: "White",
							color: "#eeeeee"
						}
					},
					mask: "mask.png"
				},
				b: {
					name: "Backsplash",
					options: {
						s: {
							name: "Subway Tile",
							color: "#e4dbce"
						},
						f: {
							name: "Floral Tile",
							color: "#bcbab3"
						},
						m: {
							name: "Marble Tile",
							color: "#d5cec8"
						},
						b: {
							name: "Beveled Subway Tile",
							color: "#e2ddd8"
						}
					},
					mask: "mask.png"
				},
				c: {
					name: "Cabinet Color",
					options: {
						g: {
							name: "Gray",
							color: "#aca4a1"
						},
						c: {
							name: "Cherry",
							color: "#65483b"
						},
						m: {
							name: "Maple",
							color: "#cbb7a1"
						},
						w: {
							name: "White",
							color: "#dfd8d8"
						}
					},
					mask: "mask.png"
				},
				a: {
					name: "Appliance Style",
					options: {
						u: {
							name: "Sunset Bronze",
							color: "#cabcad",
							suboptions: {
								u: {
									slug: "contemporary-handle",
									name: "Contemporary Handle&nbsp;& Barrel&nbsp;Knobs",
									mask: "contemporary-handle-mask.png"
								}
							}
						},
						s: {
							name: "Stainless Steel",
							color: "#b8b8b8",
							suboptions: {
								t: {
									slug: "contemporary-handle",
									name: "Contemporary Handle&nbsp;& Barrel&nbsp;Knobs",
									mask: "contemporary-handle-mask.png"
								},
								s: {
									slug: "curved-handle",
									name: "Curved Handle &&nbsp;Bladed&nbsp;Knobs",
									mask: "curved-handle-mask.png"
								}
							}
						},
						b: {
							name: "Black Stainless",
							color: "#3a3732",
							suboptions: {
								b: {
									slug: "curved-handle",
									name: "Curved Handle &&nbsp;Bladed&nbsp;Knobs",
									mask: "curved-handle-mask.png"
								}
							}
						}
					},
					suboptions: true
				}
			},
			preparedScenes: [
				{
					// a: "b",
					c: "g",
					b: "m",
					t: "w",
					f: "l"
				},
				{
					// a: "b",
					c: "w",
					b: "b",
					t: "c",
					f: "d"
				},
				{
					// a: "s",
					c: "g",
					b: "f",
					t: "d",
					f: "l"
				},
				{
					// a: "s",
					c: "c",
					b: "m",
					t: "b",
					f: "s"
				}
			],
			maxWidth: 3869,
			maxHeight: 3333
		}
	};

	angular.forEach($scope.themes, function (theme) {
		theme.orderedComponents = {};
		angular.forEach($scope.scene.componentOrder, function (key) {
			theme.orderedComponents[key] = theme.components[key];
		});
	});

	$scope.initialize = function (group) {
		$scope.selection.group = group;
		$scope.selection.step = 2;
		$scope.selection.style.open = true;
		angular.element($scope.scene.container).scrollTo(0, 0, 1000);
	};

	$scope.randomProp = function (object) {
		var keys = Object.keys(object),
			key = keys[keys.length * Math.random() << 0];
		return {
			key: key,
			value: object[key]
		};
	};

	$scope.swapStyle = function (group) {
		$scope.initialize(group == "Whirlpool Contemporary Design" ? "Whirlpool Design" : "Whirlpool Contemporary Design");
	};

	$scope.toggleMenu = function (key, $event) {
		$event.stopPropagation();
		$scope.clickWindow($event);
		if ($scope.selection.currentMenu != key)
			$scope.selection.currentMenu = key;
		else
			$scope.selection.currentMenu = null;
	};

	$scope.updateUrl = function (theme, components) {
		if (!$scope.share)
			return;
		$location.search('theme', theme);
		$location.search('design', JSON.stringify(components).replace(/\W/g, ""));
		$scope.selection.share.encoded = encodeURIComponent($location.absUrl());
	};

	$scope.shuffleSelection = function () {
		angular.forEach($scope.scene.components, function (component, key) {
			$scope.selection.components[key] = $scope.randomProp(component.options).key;
		});
	};

	var setRandomScene = function () {
		var preparedChoices = Object.keys($scope.scene.preparedScenes).filter(function (key) {
			return key !== $scope.selection.preparedScene;
		});
		$scope.selection.preparedScene = $scope.randomProp(preparedChoices).value;
		$scope.selection.components = $scope.scene.preparedScenes[$scope.selection.preparedScene];
	};

	$scope.resetSelection = function () {
		$scope.share = !/^(0|false)$/.test($location.search().share);
		$scope.selection = {
			step: 0,
			components: {a: 'u'},
			applianceStyle: {
				color: "sunset-bronze",
				handle: "contemporary-handle"
			},
			style: {},
			share: {},
			preparedScene: {},
			theme: $location.search().theme || 'traditional'
		};
		angular.extend($scope.scene, $scope.themes[$scope.selection.theme]);
		setRandomScene();
		if ($location.search().design) {
			angular.forEach($location.search().design.match(/../g), function (pair) {
				$scope.selection.components[pair[0]] = pair[1];
				if (pair[0] == 'a') {
					var option, suboption;
					angular.forEach($scope.themes[$scope.selection.theme].components.a.options, function (loopOption) {
						if (loopOption.suboptions[pair[1]]) {
							option = loopOption;
							suboption = loopOption.suboptions[pair[1]];
						}
					});
					$scope.selection.applianceStyle = {
						color: $filter('slug')(option.name),
						handle: suboption.slug
					}
				}
			});
			$scope.selection.step = 2;
			$timeout($scope.initialize);
		}
	};
	$scope.resetSelection();

	$scope.$watch("selection.applianceStyle.color", function (color) {
		var handleSlugs = Object.keys($scope.styles.handle.options);
		while (!$scope.styles.color.options[$scope.selection.applianceStyle.color].handles[$scope.selection.applianceStyle.handle]) {
			var index = handleSlugs.indexOf($scope.selection.applianceStyle.handle);
			$scope.selection.applianceStyle.handle = handleSlugs[(index + 1) % handleSlugs.length]
		}
	});

	$scope.$watchCollection("selection.applianceStyle", function (applianceStyle) {
		angular.forEach($scope.themes[$scope.selection.theme].components.a.options, function (option, key) {
			if ($filter('slug')(option.name) == applianceStyle.color) {
				angular.forEach(option.suboptions, function (suboption, suboptionKey) {
					if (suboption.slug == applianceStyle.handle)
						$scope.selection.components.a = suboptionKey;
				});
			}
		});
	});

	if (!$scope.kitchen)
		$scope.$watch("selection.step", function (step) {
			if (options.sendStepProgress)
				window.parent.postMessage({ step: step }, '*');
			if (step == 2)
				$scope.updateUrl($scope.selection.theme, $scope.selection.components);
		});

	if ($scope.controls) {
		$scope.$watchCollection("selection.components", function (components) {
			localStorage.setItem("components", JSON.stringify(components));
		});
	} else if ($scope.kitchen) {
		var setComponents = function () {
			$scope.selection.components = JSON.parse(localStorage.getItem("components"));
		};
		setComponents();
		angular.element($window).bind("storage", function () {
			$scope.$apply(setComponents);
		});
	}

	var setRatio = function () {
		var minWidth = $scope.scene.minWidth,
			minHeight = $scope.scene.minHeight,
			maxWidth = $scope.scene.maxWidth,
			maxHeight = $scope.scene.maxHeight,
			containerHeight = $scope.scene.container.clientHeight,
			containerWidth = window.innerWidth < 768 && window.innerHeight > window.innerWidth ? Math.round(containerHeight * minWidth / minHeight) : $scope.scene.container.offsetWidth;

		if (containerWidth / containerHeight > minWidth / minHeight) {
			var naturalWidth = Math.min(containerWidth / containerHeight * minHeight, maxWidth);
			var naturalHeight = naturalWidth * containerHeight / containerWidth;
		} else {
			var naturalHeight = Math.min(containerHeight / containerWidth * minWidth, maxHeight);
			var naturalWidth = naturalHeight * containerWidth / containerHeight;
		}

		if (
			window.innerWidth > 1024 && (
				maxWidth / naturalWidth * containerWidth * (devicePixelRatio || 1) > maxWidth / 2 ||
				maxHeight / naturalHeight * containerHeight * (devicePixelRatio || 1) > maxHeight / 2
			)
		)
			var highRes = true;
		$scope.scene.resSuffix = highRes ? '@2x' : '';

		$scope.scene.sWidth = naturalWidth / maxWidth;
		$scope.scene.sHeight = naturalHeight / maxHeight;
		$scope.scene.sx = (maxWidth - naturalWidth) / 2 / maxWidth;
		$scope.scene.sy = (maxHeight - naturalHeight) / 2 / maxHeight;
		$scope.scene.dWidth = containerWidth * (devicePixelRatio || 1);
		$scope.scene.dHeight = containerHeight * (devicePixelRatio || 1);

		createCanvas();
	};

	var createCanvas = function () {
		var loadCanvasImage = function (src) {
			return $q(function (resolve, reject) {
				var image = new Image();
				image.addEventListener('load', function () {
					resolve(image);
				});
				image.addEventListener('error', reject);
				image.src = src;
			})
		};

		var getBase = loadCanvasImage('images/' + $scope.selection.theme + '/components/base' + ($scope.scene.resSuffix || '') + '.jpg');
		// Remove this block when both themes have the same size images
		getBase = getBase.then(function (success) {
			var compCanvas = document.createElement('canvas');
			compCanvas.width = Math.round(success.height * 3869 / 3333);
			compCanvas.height = success.height;
			var compContext = compCanvas.getContext('2d');
			compContext.drawImage(success, 0, 0);
			return compCanvas;
		});
		var canvasArray = [getBase];

		angular.forEach($scope.selection.components, function (optionKey, key) {
			var component = $scope.scene.components[key];
			var compCanvas = document.createElement('canvas');

			var getImage, getMask;
			if (!component.suboptions) {
				getImage = loadCanvasImage('images/' + $scope.selection.theme + '/components/' + $filter('slug')(component.name) + '/' + $filter('slug')(component.options[optionKey].name) + ($scope.scene.resSuffix || '') + '.jpg');
				getMask = loadCanvasImage('images/' + $scope.selection.theme + '/components/' + $filter('slug')(component.name) + '/' + (component.mask || component.options[optionKey].mask));
			} else {
				var option, suboption;
				angular.forEach(component.options, function (loopOption) {
					if (loopOption.suboptions[optionKey]) {
						option = loopOption;
						suboption = loopOption.suboptions[optionKey];
					}
				});
				getImage = loadCanvasImage('images/' + $scope.selection.theme + '/components/' + $filter('slug')(component.name) + '/' + $filter('slug')(option.name) + '-' + suboption.slug + ($scope.scene.resSuffix || '') + '.jpg');
				getMask = loadCanvasImage('images/' + $scope.selection.theme + '/components/' + $filter('slug')(component.name) + '/' + (component.mask || suboption.mask));
			}

			canvasArray.push(
				$q.all([getImage, getMask]).then(function (success) {
					// Restore these comments when both themes have the same size images
					// compCanvas.width = success[0].width;
					compCanvas.width = Math.round(success[0].height * 3869 / 3333);
					compCanvas.height = success[0].height;
					var compContext = compCanvas.getContext('2d');
					compContext.globalCompositeOperation = "destination-atop";
					compContext.drawImage(success[0], 0, 0);
					// compContext.drawImage(success[1], 0, 0, compCanvas.width, compCanvas.height);
					compContext.drawImage(success[1], 0, 0, success[0].width, compCanvas.height);
					return compCanvas;
				})
			);
		});

		$q.all(canvasArray).then(function (success) {
			var sceneCanvas = document.createElement('canvas');
			sceneCanvas.width = $scope.scene.dWidth;
			sceneCanvas.height = $scope.scene.dHeight;
			sceneCanvas.className = 'scene';

			var sceneContext = sceneCanvas.getContext('2d');
			angular.forEach(success, function (compCanvas) {
				sceneContext.drawImage(
					compCanvas,
					$scope.scene.sx * compCanvas.width,
					$scope.scene.sy * compCanvas.height,
					$scope.scene.sWidth * compCanvas.width,
					$scope.scene.sHeight * compCanvas.height,
					0,
					0,
					$scope.scene.dWidth,
					$scope.scene.dHeight
				);
				compCanvas.width = 0;
				compCanvas.height = 0;
			});

			var $sceneContainer = angular.element(document.querySelector('.scene-container'));
			var $oldScenes = $sceneContainer.children();

			$sceneContainer.prepend(sceneCanvas);

			$oldScenes.on('transitionend', function () {
				$oldScenes.remove();
				angular.forEach($oldScenes, function (scene) {
					scene.width = 0;
					scene.height = 0;
				});
			});
			$oldScenes.addClass('fade ng-leave ng-leave-active');

			if (!$scope.selection.style.open)
				$timeout(function () {
					$scope.scene.container.scrollLeft = 9999;
				});
		});
	};

	$timeout(function () {
		$scope.scene.container = document.querySelector(".scene-container");
		$scope.scene.minWidth = 3000;
		$scope.scene.minHeight = 2000;
		$scope.$watchCollection("selection.components", function (components) {
			setRatio();
			if ($scope.selection.step === 2)
				$scope.updateUrl($scope.selection.theme, components);
		});
		$scope.$watch("selection.theme", function (newTheme, oldTheme) {
			if (oldTheme === newTheme) return;
			$scope.themes[oldTheme].selectionComponents = angular.copy($scope.selection.components);
			angular.extend($scope.scene, $scope.themes[newTheme]);
			if ($scope.themes[newTheme].selectionComponents)
				$scope.selection.components = angular.copy($scope.themes[newTheme].selectionComponents);
			else
				setRandomScene();
			$scope.selection.components.a = $scope.themes[oldTheme].selectionComponents.a;
			setRatio();
			if ($scope.selection.step === 2)
				$scope.updateUrl(newTheme, $scope.selection.components);
		});
	});

	$scope.clickWindow = function (event) {
		window.dispatchEvent(new MouseEvent(event.type, event));
	};

	$scope.windowSize = [window.innerWidth, window.innerHeight];

	resize.init({
		scope: $scope,
		resizedFn: function () {
			if ($scope.windowSize.join() === [window.innerWidth, window.innerHeight].join()) return;
			$scope.$apply(setRatio);
			$scope.windowSize = [window.innerWidth, window.innerHeight];
		}
	});

	$scope.logo = true;
	angular.element($window).bind('message', function (event) {
		if ('logo' in (event.data || {}))
			$scope.$apply('logo = ' + event.data.logo)
	});
});

showroom.directive("preload", function () {
	return {
		link: function ($scope, $element, $attrs) {
			// Don't show this duplicate element
			$element[0].style.display = "none";
			// When the image loads
			$element.on("load", function () {
				$scope.$apply(function () {
					// Set the real image to this URI
					$scope.$eval($attrs.preload + " = '" + $attrs.ngSrc + "'");
					if ($attrs.preloadSrcset)
						$scope.$eval($attrs.preloadSrcset + " = '" + $attrs.ngSrcset + "'");
				});
			});
		}
	};
});

showroom.directive("safariRefreshSvg", function ($timeout) {
	return {
		link: function ($scope, $element, $attrs) {
			var $scene = angular.element(document.querySelector('.scene'));
			$scene.parent().append($scene);
		}
	};
})

showroom.directive("buttonBubble", function () {
	return {
		link: function ($scope, $button) {
			var button = $button[0];
			if (getComputedStyle(button).position === 'static')
				$button.css({ position: 'relative' });
			if (getComputedStyle(button).zIndex === 'auto')
				$button.css({ zIndex: 0 });
			$button.css({ overflow: 'hidden' });
			var $bubble = angular.element('<span>').addClass('button-oval__bubble');
			$button.append($bubble);
			button.addEventListener('mouseleave', positionBubble);
			button.addEventListener('mouseenter', positionBubble);
			button.addEventListener('focusin', centerBubble);
			button.addEventListener('focusout', centerBubble);

			function positionBubble(event) {
				var offsetX = event.offsetX;
				var offsetY = event.offsetY;
				var xRadius = Math.max(offsetX, button.offsetWidth - offsetX);
				var yRadius = Math.max(offsetY, button.offsetHeight - offsetY);
				var radius = Math.sqrt(xRadius * xRadius + yRadius * yRadius);
				$bubble.css({
					left: offsetX + 'px',
					top: offsetY + 'px',
					padding: radius + 'px'
				});
			}

			function centerBubble() {
				positionBubble({
					offsetX: button.offsetWidth / 2,
					offsetY: button.offsetHeight / 2
				});
			}
		}
	};
});

showroom.filter("sanitize", function ($sce) {
	return $sce.trustAsHtml;
});

showroom.filter("contains", function () {
	return function (array, needle) {
		return array.indexOf(needle) >= 0;
	};
});

showroom.filter("slug", function () {
	return function(input) {
		return String(input).replace(/\s+/g, "-").replace(/[^\w-]/g, "").toLowerCase();
	}
});

var options = {
	preventWheel: false,
	sendClickEvent: false,
	sendWheelEvent: false,
	sendStepProgress: false
};
window.addEventListener('click', function (event) {
	if (options.sendClickEvent)
		parent.postMessage({
			type: event.type,
			screenX: event.screenX,
			screenY: event.screenY
		}, '*');
})
window.addEventListener('wheel', function (event) {
	if (options.preventWheel)
		event.preventDefault();
	if (options.sendWheelEvent)
		parent.postMessage({
			type: event.type,
			deltaX: event.deltaX,
			deltaY: event.deltaY
		}, '*');
});
window.addEventListener('message', function (event) {
	Object.keys(options).forEach(function (key) {
		if (key in (event.data || {}))
			options[key] = event.data[key];
	});
});
if (window.self !== window.top)
	setTimeout(function () {
		window.parent.postMessage({ ready: true }, '*');
	}, 1000);
